#include <QDataStream>
#include <QObject>
#include <QNearFieldManager>
#include <QDebug>
#include "nfcaccess.h"

using namespace QtMobility;

NfcAccess::NfcAccess(QObject *parent) :
    QNearFieldManager()
{
    strSNR="--------";
    setTargetAccessModes(QNearFieldManager::NoTargetAccess);

//    setTargetAccessModes(QNearFieldManager::TagTypeSpecificTargetAccess);

    QObject::connect(this, SIGNAL(targetDetected(QNearFieldTarget*)),
                     this, SLOT(onTarget(QNearFieldTarget *)));

    if (startTargetDetection(QNearFieldTarget::MifareTag))
        qDebug("started...");
}


void
NfcAccess::onTarget(QNearFieldTarget *target)
{
    //qDebug("target %X:%X:%X:%X", target->uid().at(0), target->uid().at(1), target->uid().at(2), target->uid().at(3));
    if(target->uid().size()<4)
        strSNR=QString("UID fail!?");
    else
        strSNR=QString("%1%2%3%4").arg(target->uid().at(0), 2, 16, QChar('0')).arg(target->uid().at(1), 2, 16, QChar('0')).arg(target->uid().at(2), 2, 16, QChar('0')).arg(target->uid().at(3), 2, 16, QChar('0')).toUpper();

    connect(target, SIGNAL(error(QNearFieldTarget::Error,QNearFieldTarget::RequestId)), this, SLOT(onTargetError(QNearFieldTarget::Error,QNearFieldTarget::RequestId)));
    emit tagFound();
    target->deleteLater();
    stopTargetDetection();

    startTargetDetection();
}

void
NfcAccess::onTargetError(QNearFieldTarget::Error,QNearFieldTarget::RequestId)
{
    qDebug() << "target error";
}
