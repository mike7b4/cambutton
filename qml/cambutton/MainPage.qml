import QtQuick 1.1
import com.nokia.meego 1.0

Page {
    id: page1
    tools: commonTools

    TopPanel{
        id: topPanel
        strLogo: "CamButton version 0.3.2"
    }

    state: (screen.currentOrientation == Screen.Portrait) ? "portrait" : "landscape"

        states: [
            State {
                name: "landscape"
                PropertyChanges { target: rectangle1; anchors.topMargin: 12;  }
                PropertyChanges { target: topText; height: 70;  }
            },
            State {
                name: "portrait"
                PropertyChanges { target: rectangle1; anchors.topMargin: 32;  }
                PropertyChanges { target: topText; height: 100;  }
            }
        ]



    Item{
        id: rectangle1
        anchors.top: topPanel.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        Column{

            id: column1
            width: parent.width
            anchors.top: topPanel.bottom
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.left: parent.left

            anchors.margins: 16

            spacing: 10
            Item {
                id: topText
                width: parent.width
                height: 70
                Text{
                    width: parent.width
                    font.pixelSize: 32
                    color: "white"
                    text: "Copyright 2012-2013\nMikael Hermansson"
                    verticalAlignment: Text.AlignTop
                    horizontalAlignment: Text.AlignHCenter
                }
            }
            Text{
                font.pixelSize: 32
                wrapMode: Text.WordWrap
                width: parent.width
                color: "cyan"
                text: "This application licensed under GPL2! Code and idea heavily based on thp's camerra app. But rewritted in C++ and statically linked with xresponse.c library. How the app works, simply swipe to cameraapp and take a photo with the volumeup button or Tap with a Tag."
                horizontalAlignment: Text.AlignHCenter
                anchors.horizontalCenter: Text.AlignHCenter
            }
            CheckBox {
                id: active
                objectName: "active"
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Active"
                checked: true
                visible: false
            }
        }
        MouseArea{
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: pageStack.pop()
        }
    }

}
