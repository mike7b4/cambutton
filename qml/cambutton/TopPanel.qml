import QtQuick 1.1
import com.nokia.meego 1.0
Item{
    height: 70
//    width: parent.width
    anchors.left: parent.left
    anchors.right: parent.right
    z: 1
    property string strLogo: ""
    Rectangle {
        id: radTop
        height: 10
        width: parent.width
        color: "darkcyan"
        anchors.topMargin: 0
        radius: 10
    }
    Rectangle {
        height: 63
        anchors.top: radTop.bottom-3
        color: "darkcyan"
        width: parent.width
/*
        gradient: Gradient {
              GradientStop { position: 0.1; color: "darkcyan" }
              GradientStop { position: 1.0; color: "cyan" }
        }
        */
        Label {
            text: strLogo
            color: "#CCCCCC"
            font.pixelSize: 32
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }
    }
}
