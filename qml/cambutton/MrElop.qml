// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.0

Page {
    Item{

        state: (screen.currentOrientation == Screen.Portrait) ? "portrait" : "landscape"

            states: [
                State {
                    name: "landscape"
                    PropertyChanges { target: elopIsAFool; font.pixelSize: 64; text: "Mr Elop! You are an evil"  }
                    PropertyChanges { target: elopIsAFool2;  visible: false  }
                    PropertyChanges { target: maemo; text: "<center>Maemo Harmattan</center><center>and Qt</center>" }
                },
                State {
                    name: "portrait"
                    PropertyChanges { target: elopIsAFool; font.pixelSize: 72; text: "Mr Elop!" }
                    PropertyChanges { target: elopIsAFool2; font.pixelSize: 64; text: "You are an evil"; visible: true  }
                    PropertyChanges { target: maemo; text: "<center>Maemo Harmattan</center><center>and Qt</center>" }
                }
            ]

        width: parent.width
        height: parent.height
        Column{

            id: column1
            anchors.centerIn: parent
            spacing: 5
            Text {
                id: elopIsAFool
                font.pixelSize:  64
                color: "chartreuse"
                text: "Mr Elop, you are an evil"
                anchors.horizontalCenter: parent.horizontalCenter

            }
            Text {
                id: elopIsAFool2
                font.pixelSize:  64
                color: "chartreuse"
                text: "You are an evil"
                anchors.horizontalCenter: parent.horizontalCenter
                visible: false
            }
            Text {
                id: elopIsAMicrosoftTroll
                font.pixelSize: 64
                color: "chartreuse"
                text: "<center>Microsoft Troll.</center>"
                anchors.horizontalCenter: parent.horizontalCenter

            }


            Text {
                id: maemo
                font.pixelSize: 48
                color: "magenta"
                anchors.horizontalCenter: parent.horizontalCenter

            }
            Text {
                font.pixelSize: 48
                color: "cyan"
                anchors.horizontalCenter: parent.horizontalCenter
                text: "<u>Rocks!</u>"

            }
        }
        MouseArea{
            anchors.fill: parent
            onClicked: pageStack.pop()
        }
    }
    /*
    ToolBarLayout {
        id: backTools
        visible: true
        ToolIcon {
                 iconId: "toolbar-back";
                 onClicked: pageStack.pop()
        }
    }
    */
}
