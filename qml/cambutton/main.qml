import QtQuick 1.1
import com.nokia.meego 1.0

PageStackWindow {
    id: appWindow

    initialPage: mainPage

    MainPage {
        id: mainPage
    }
    Component.onCompleted: {
        theme.inverted = true;

    }
    platformStyle: PageStackWindowStyle {
//             landscapeBackground: "backgroundImageLandscape.png"
       //      portraitBackground: "file://opt/uglyradio/images/cross.png"
            background: "image://theme/480x854"
             backgroundFillMode: Image.Stretch
         }

    ToolBarLayout {
        id: commonTools
        visible: false
    }

}
