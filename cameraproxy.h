#ifndef CAMERAPROXY_H
#define CAMERAPROXY_H

#include <QObject>
#include <QTimer>
#include <QDebug>
#include <QDataStream>
#include <QDeclarativeView>
#include <qmsystem2/qmkeys.h>
#include <QDeclarativeItem>
#include <QDeclarativeContext>
#include <X11/Xlib.h>
#include "nfcaccess.h"
/* looks like xlib defines this and it make Qt includes go nuts */
#undef Status

//using namespace MeeGo;
//using namespace QDeclarativeView;
class CameraProxy : public QObject
{
    Q_OBJECT
    QTimer timer;
    MeeGo::QmKeys keys;
    int state;
    Display *display;
    int screen;
    NfcAccess nfc;
    QDeclarativeItem *checkboxActive;
    QDeclarativeView *view;
public:
    explicit CameraProxy(Display *display, int screen);
    /* raw X11 */
    Window  getActiveApp();
    int isCameraApp(Window winid );
    void setViewer(QDeclarativeView *view);
signals:

public slots:
    void onTimeout();
    void onKeyEvent(MeeGo::QmKeys::Key a,MeeGo::QmKeys::State b);
    void onTagFound();
    void onContextChange(QDeclarativeView::Status s);
};

#endif // CAMERAPROXY_H
