#ifndef NFCACCESS_H
#define NFCACCESS_H
#include <QDataStream>


#include <QObject>
#include <QNearFieldManager>
#include <QNearFieldTarget>
using namespace QtMobility;

class NfcAccess : public QtMobility::QNearFieldManager
{
    Q_OBJECT
    QString strSNR;

public:
    explicit NfcAccess(QObject *parent = 0);

signals:
    void tagFound();
public slots:
    QString getData(){ return strSNR; };
    void onTarget(QNearFieldTarget *target);
    void onTargetError(QNearFieldTarget::Error,QNearFieldTarget::RequestId);
};

#endif // nfcaccesS

