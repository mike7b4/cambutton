#include <QtDBus/QtDBus>
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusReply>
#include <QThread>
#include <QProcess>
#include <QDeclarativeView>
#include <X11/Xlib.h>
#undef Status
//#include <QDeclarativeContext>
#include <stdint.h>
#include "nfcaccess.h"
#include "cameraproxy.h"
extern "C" {
//typedef int Bool;
//typedef int Time;
//typedef int Window;
Bool wait_response(Display *dpy);
Time drag_event(Display *dpy, int x, int y, int button_state);
Time fake_event(Display *dpy, int x, int y);

void setup_display(Display *dpy);
enum { /* for 'dragging' */
  XR_BUTTON_STATE_NONE,
  XR_BUTTON_STATE_PRESS,
  XR_BUTTON_STATE_RELEASE
};
}

using namespace MeeGo;
CameraProxy::CameraProxy(Display *d, int s) :
    QObject(NULL),
    timer(),
    keys(),
    display(d),
    screen(s),
    nfc(),
    checkboxActive(NULL),
    view(NULL)
{
    state=1;
    connect(&timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    connect(&keys, SIGNAL(keyEvent(MeeGo::QmKeys::Key,MeeGo::QmKeys::State)), this, SLOT(onKeyEvent(MeeGo::QmKeys::Key,MeeGo::QmKeys::State)));
    /* this is to make sure were up before camera */
    timer.start(10);
    timer.setSingleShot(true);
    connect(&nfc, SIGNAL(tagFound()), this, SLOT(onTagFound()));

    setup_display(display);
}

void
CameraProxy::onKeyEvent(MeeGo::QmKeys::Key key,MeeGo::QmKeys::State kstate)
{
    if ((key==MeeGo::QmKeys::VolumeUp || key==MeeGo::QmKeys::VolumeDown) &&
        kstate==MeeGo::QmKeys::KeyDown &&
        state==2)
    {
       /* if cambutton is active when pressing button we simply call cameraapp instead */
       if(view->isActiveWindow())
       {
           QDBusInterface iface("com.nokia.maemo.CameraService", "/org/maemo/m", "com.nokia.MApplicationIf");
           iface.call("launch");
           /* dirty wait one sec */
           ::sleep(1);
           /* .. hopefully this means camera app has now got focus so we can continue below */
       }
       /* check if camapp is focus */
       Window winid=getActiveApp();
       if (isCameraApp(winid)) {
           state=3;
           drag_event(display, 820, 240, XR_BUTTON_STATE_PRESS);
        /* dirty dont wait for response here but if user hold down but more than five secs go back to state 0 */
           timer.start(5000);
       }
    }
    if ((key==MeeGo::QmKeys::VolumeUp || key==MeeGo::QmKeys::VolumeDown) &&
       kstate==MeeGo::QmKeys::KeyUp &&
       state==3)
    {
//          Window winid=getActiveApp();
  //        if (isCameraApp(winid)) {
         drag_event(display, 820, 240, XR_BUTTON_STATE_RELEASE);
         wait_response(display);
    //      }
          state=0;
          /* after taking a photo wait atleast 200 ms */
          timer.start(200);
    }

}

void
CameraProxy::onTagFound()
{
    Window winid=getActiveApp();
    if (isCameraApp(winid)) {
        drag_event(display, 820, 240, XR_BUTTON_STATE_PRESS);
        drag_event(display, 820, 240, XR_BUTTON_STATE_RELEASE);
        wait_response(display);
    }
 }

void
CameraProxy::onTimeout()
{
    if (state==1) {
        QDBusInterface iface("com.nokia.maemo.CameraService", "/org/maemo/m", "com.nokia.MApplicationIf");
        iface.call("launch");
        state=2;
    }
    else if (state==3) /* means button down tiemout */
    {
        /* fake release */
        state=2;
        drag_event(display, 820, 240, XR_BUTTON_STATE_RELEASE);
        wait_response(display);
    }
    else if (state==0)
        state=2;
}

Window
CameraProxy::getActiveApp()
{
  unsigned char *name=NULL;
  int nprops=0;
  int format=0;
  Atom type=0;
  long unsigned int nums=0;
  Atom *atoms=NULL;
  long unsigned int sec=0;
  int b=0;
  Window root,windowid;
  windowid=root=0;
  root=XRootWindow(display, screen);
  atoms=XListProperties(display, root,&nprops);

  for ( b=0;b<nprops;b++) {
    name= (unsigned char*)XGetAtomName(display, atoms[b]);
    if (strstr((char*)name, "_NET_ACTIVE_WINDOW")!=NULL) {
      XFree(name);
      Window *ptr;
      XGetWindowProperty(display, root, atoms[b], 0, 1,FALSE,AnyPropertyType,&type,&format,&nums,&sec, (unsigned char**)&ptr);
      if (format==32)
    windowid=*ptr;

      XFree(ptr);
      /* we got the Active Window so stop iterate */
      break;
    }
    /* fre the malloced object */
    XFree(name);
  }

  if (nprops)
    XFree(atoms);

  return windowid;
}

int
CameraProxy::isCameraApp( Window winid )
{
  int iswin=FALSE;
  unsigned char *name=NULL;
  int nprops=0;
  int format=0;
  Atom type=0;
  long unsigned int nums=0;
  Atom *atoms=NULL;
  long unsigned int sec=0;
  int b=0;
  Window root,windowid;
  windowid=root=0;
//  root=XRootWindow(display, screen);
  atoms=XListProperties(display, winid,&nprops);

  for ( b=0;b<nprops;b++) {
      name=(unsigned char*) XGetAtomName(display, atoms[b]);
    if (strstr((char*)name, "WM_CLASS")!=NULL) {
      XFree(name);
      /* reuse name */

      XGetWindowProperty(display, winid, atoms[b], 0, -1,FALSE,AnyPropertyType,&type,&format,&nums,&sec, &name);
      qWarning() << name << endl;
      if (name && strstr((char*)name, "camera-ui"))
      {
    iswin=TRUE;
      }
      XFree(name);
      /* we got the Active Window so stop iterate */
      break;

    }
    /* fre the malloced object */
    XFree(name);
  }

  if (nprops)
    XFree(atoms);

  return iswin;
}

void
CameraProxy::onContextChange(QDeclarativeView::Status stat)
{
    qDebug() << "=========================here";
}

void
CameraProxy::setViewer(QDeclarativeView *v)
{
    QDeclarativeContext *root;
    view=v;
    root=view->rootContext();
    qDebug() << "==" << root;

    checkboxActive=root->findChild<QDeclarativeItem*>("active");

    qDebug() << "FREAK " << checkboxActive;
    if (checkboxActive==NULL)
        connect(view,SIGNAL(statusChanged(QDeclarativeView::Status)), this, SLOT(onContextChange(QDeclarativeView::Status)));


}
