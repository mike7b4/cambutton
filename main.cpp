#include <QtGui/QApplication>
#include <QDeclarativeContext>
#include <QX11Info>

#include "qmlapplicationviewer.h"
#include "cameraproxy.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    int res=0;
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    //QDBusReply reply=

    QmlApplicationViewer viewer;
    CameraProxy *proxy=new CameraProxy(QX11Info::display(), QX11Info::appScreen());
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/cambutton/main.qml"));
    proxy->setViewer(&viewer);
    viewer.showExpanded();
    viewer.hide();
    viewer.showMinimized();

    res=app->exec();

    return res;
}
